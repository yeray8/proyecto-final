import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PremiumComponent } from './components/premium/premium.component';
import { HeroComponent } from './components/hero/hero.component';
import { DetallesEventoComponent } from './components/detalles-evento/detalles-evento.component';
import { ListaEventosComponent } from './components/admin/lista-eventos/lista-eventos.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegistroComponent } from './components/user/registro/registro.component';
import { PerfilComponent } from './components/user/perfil/perfil.component';
import { Pagina404Component } from './components/pagina404/pagina404.component';

import { HttpClientModule } from '@angular/common/http';

// Services
import { DataApiService } from './services/data-api.service';
import { FormsModule } from '@angular/forms';
import { ModalComponent } from './components/modal/modal.component';
import { TruncateTextPipe } from './pipes/truncate-text-pipe';

import { AgendaComponent } from './components/agenda/agenda.component';
import { ArteComponent } from './components/arte/arte.component';
import { MusicaComponent } from './components/musica/musica.component';
import { CineComponent } from './components/cine/cine.component';
import { GastronomiaComponent } from './components/gastronomia/gastronomia.component';
import { InfantilComponent } from './components/infantil/infantil.component';
import { LocalesComponent } from './components/locales/locales.component';
import { GuiaComponent } from './components/guia/guia.component';
import { FooterComponent } from './components/footer/footer.component';

//Externo
import {NgxPaginationModule} from 'ngx-pagination'; 
import { NgxSpinnerModule} from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    PremiumComponent,
    HeroComponent,
    DetallesEventoComponent,
    ListaEventosComponent,
    LoginComponent,
    RegistroComponent,
    PerfilComponent,
    Pagina404Component,
    ModalComponent,
    TruncateTextPipe,
    AgendaComponent,
    ArteComponent,
    MusicaComponent,
    CineComponent,
    GastronomiaComponent,
    InfantilComponent,
    LocalesComponent,
    GuiaComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    NgxSpinnerModule
  ],
  providers: [DataApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
