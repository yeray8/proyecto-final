import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { EventoInterface } from '../models/evento-interface'

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  constructor( private http: HttpClient, private authService: AuthService) { }

  eventos: Observable<any>;
  evento: Observable<any>;

  public selectedEvento: EventoInterface ={
    id: null,
    titulo:'',
    descripcion:'',
    categoria: '',
    imagen: ''
  };

  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: this.authService.getToken(),
  })

  getAllEventos(){
    const url_api = "http://localhost:3000/api/eventos";
    return this.http.get(url_api);
  }

  getAgenda(){
    const url_api = "http://localhost:3000/api/eventos?filter[where][categoria]=agenda";
    return this.http.get(url_api);
  }

  getArte(){
    const url_api = "http://localhost:3000/api/eventos?filter[where][categoria]=arte";
    return this.http.get(url_api);
  }

  getMusica(){
    const url_api = "http://localhost:3000/api/eventos?filter[where][categoria]=musica";
    return this.http.get(url_api);
  }

  getCine(){
    const url_api = "http://localhost:3000/api/eventos?filter[where][categoria]=cine";
    return this.http.get(url_api);
  }

  getGastronomia(){
    const url_api = "http://localhost:3000/api/eventos?filter[where][categoria]=gastronomia";
    return this.http.get(url_api);
  }

  getInfantil(){
    const url_api = "http://localhost:3000/api/eventos?filter[where][categoria]=infantil";
    return this.http.get(url_api);
  }

  getLocales(){
    const url_api = "http://localhost:3000/api/eventos?filter[where][categoria]=locales";
    return this.http.get(url_api);
  }

  getGuia(){
    const url_api = "http://localhost:3000/api/eventos?filter[where][categoria]=guia";
    return this.http.get(url_api);
  }

  getNoPremium(){
    const url_api = "http://localhost:3000/api/eventos?filter[where][categoria][neq]=premium&filter[order]=id DESC";
    return this.http.get(url_api);
  }

  getEventoById(id: string){
    const url_api = `http://localhost:3000/api/eventos/${id}`;
    return (this.evento = this.http.get(url_api));
  }

  getPremium(){
    const url_api = `http://localhost:3000/api/eventos?filter[where][categoria]=premium`;
    return (this.eventos = this.http.get(url_api));
  }

  saveEvento(evento: EventoInterface){
    const token = this.authService.getToken();
    const url_api = `http://localhost:3000/api/eventos?access_token=${token}`;
    return this.http.post<EventoInterface>(url_api, evento,{headers: this.headers})
    .pipe(map(data => data));
  }

  updateEvento(evento){
    const eventoId = evento.eventoId;
    const token = this.authService.getToken();
    const url_api = `http://localhost:3000/api/eventos/${eventoId}/?access_token=${token}`;
    return this.http.put<EventoInterface>(url_api, evento,{headers: this.headers})
    .pipe(map(data => data));
  }

  deleteEvento(id: string){
    const token = this.authService.getToken();
    console.log(token);
    const url_api = `http://localhost:3000/api/eventos/${id}/?access_token=${token}`;
    return this.http.delete<EventoInterface>(url_api, {headers: this.headers})
    .pipe(map(data => data));

  }

}
