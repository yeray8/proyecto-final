import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PremiumComponent } from './components/premium/premium.component';
import { DetallesEventoComponent } from './components/detalles-evento/detalles-evento.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegistroComponent } from './components/user/registro/registro.component';
import { PerfilComponent } from './components/user/perfil/perfil.component';
import { ListaEventosComponent } from './components/admin/lista-eventos/lista-eventos.component';
import { Pagina404Component } from './components/pagina404/pagina404.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { ArteComponent } from './components/arte/arte.component';
import { CineComponent } from './components/cine/cine.component';
import { GastronomiaComponent } from './components/gastronomia/gastronomia.component';
import { GuiaComponent } from './components/guia/guia.component';
import { InfantilComponent } from './components/infantil/infantil.component';
import { LocalesComponent } from './components/locales/locales.component';
import { MusicaComponent } from './components/musica/musica.component';

import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {path: '', component: HomeComponent},
  { path: 'agenda', component:AgendaComponent},
  { path: 'arte', component:ArteComponent},
  { path: 'cine', component:CineComponent},
  { path: 'musica', component:MusicaComponent},
  { path: 'gastronomia', component:GastronomiaComponent},
  { path: 'infantil', component:InfantilComponent},
  { path: 'guia', component:GuiaComponent},
  { path: 'locales', component:LocalesComponent},
  { path: 'premium', component: PremiumComponent, canActivate:[AuthGuard]},
  { path: 'evento/:id', component: DetallesEventoComponent },
  { path: 'admin/lista-eventos', component: ListaEventosComponent, canActivate:[AuthGuard]},
  { path: 'user/login', component: LoginComponent },
  { path: 'user/registro', component: RegistroComponent },
  { path: 'user/perfil', component: PerfilComponent, canActivate:[AuthGuard]},
  { path: '**', component: Pagina404Component }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
