import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { EventoInterface } from "../../models/evento-interface";

@Component({
  selector: 'app-guia',
  templateUrl: './guia.component.html',
  styleUrls: ['./guia.component.css']
})
export class GuiaComponent implements OnInit {

  constructor( private dataApi: DataApiService) { }

  private eventos: EventoInterface;

  ngOnInit() {
    this.getListEventos();
  }

  getListEventos(){
    this.dataApi.getGuia().subscribe((eventos: EventoInterface) => (this.eventos = eventos));
  }

}
