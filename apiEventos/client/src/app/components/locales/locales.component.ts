import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { EventoInterface } from "../../models/evento-interface";

@Component({
  selector: 'app-locales',
  templateUrl: './locales.component.html',
  styleUrls: ['./locales.component.css']
})
export class LocalesComponent implements OnInit {

  constructor( private dataApi: DataApiService) { }

  private eventos: EventoInterface;

  ngOnInit() {
    this.getListEventos();
  }

  getListEventos(){
    this.dataApi.getLocales().subscribe((eventos: EventoInterface) => (this.eventos = eventos));
  }

}
