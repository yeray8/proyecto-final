import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { EventoInterface } from '../../../models/evento-interface';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-lista-eventos',
  templateUrl: './lista-eventos.component.html',
  styleUrls: ['./lista-eventos.component.css']
})
export class ListaEventosComponent implements OnInit {

  constructor(private dataApiService: DataApiService) { }

  private eventos: EventoInterface;
  p: number =1;
  public myCounter: number = 0;

  ngOnInit() {
    this.getListEventos();
  }

  getListEventos(): void{
    this.dataApiService
    .getAllEventos()
    .subscribe((eventos: EventoInterface)=>(this.eventos = eventos));
  }

  onDeleteEvento(id: string): void {
    if (confirm('¿Está seguro de eliminar?')) {
      this.dataApiService.deleteEvento(id).subscribe();
    }
  }

  onPreUpdateEvento(evento: EventoInterface): void {
    this.dataApiService.selectedEvento = Object.assign({}, evento);
  }

  resetForm(eventoForm?: NgForm): void {
    this.dataApiService.selectedEvento = {
      id: null,
      titulo: '',
      descripcion: '',
      categoria: ''
    };
  }

}
