import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { ActivatedRoute, Params } from '@angular/router';
import { EventoInterface } from 'src/app/models/evento-interface';

@Component({
  selector: 'app-detalles-evento',
  templateUrl: './detalles-evento.component.html',
  styleUrls: ['./detalles-evento.component.css']
})
export class DetallesEventoComponent implements OnInit {

  constructor(private dataApi: DataApiService, private route: ActivatedRoute) { }

  private evento: EventoInterface = {
    titulo: '',
    descripcion: '',
    imagen: '',
    categoria: ''
  }

  ngOnInit() {
    const evento_id = this.route.snapshot.params['id'];
    this.getDetalles(evento_id);
  }

  getDetalles(id: string) {
    this.dataApi.getEventoById(id).subscribe(evento => (this.evento = evento));
  }
}
