import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserInterface } from 'src/app/models/user-interface';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private location: Location) { }

  private user: UserInterface = {
    name: "",
    email: "",
    password: ""
  };

  ngOnInit() {
  }

  onRegistro(): void{
    this.authService.registrarUser(this.user.name, this.user.email, this.user.password)
    .subscribe(user =>{
      this.authService.setUser(user);
      const token = user.id;
      this.authService.setToken(token);
      this.router.navigate(["/user/perfil"]);
      //location.reload();
    });
  }
}
