import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { EventoInterface } from "../../models/evento-interface";

@Component({
  selector: 'app-musica',
  templateUrl: './musica.component.html',
  styleUrls: ['./musica.component.css']
})
export class MusicaComponent implements OnInit {

  constructor( private dataApi: DataApiService) { }

  private eventos: EventoInterface;

  ngOnInit() {
    this.getListEventos();
  }

  getListEventos(){
    this.dataApi.getMusica().subscribe((eventos: EventoInterface) => (this.eventos = eventos));
  }

}
