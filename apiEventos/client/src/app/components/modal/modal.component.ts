import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { EventoInterface } from '../../models/evento-interface';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  constructor(private dataApiService: DataApiService, private location: Location) { }

  ngOnInit() {
  }

  onSaveEvento(eventoForm: NgForm): void {
    if (eventoForm.value.eventoId == null) {
      // new
      this.dataApiService.saveEvento(eventoForm.value).subscribe(evento => location.reload());
    } else {
      // update
      this.dataApiService.updateEvento(eventoForm.value).subscribe(evento => location.reload());
    }
  }

}
