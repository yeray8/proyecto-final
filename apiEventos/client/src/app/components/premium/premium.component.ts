import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { EventoInterface } from 'src/app/models/evento-interface';

@Component({
  selector: 'app-premium',
  templateUrl: './premium.component.html',
  styleUrls: ['./premium.component.css']
})
export class PremiumComponent implements OnInit {

  constructor(private dataApi: DataApiService) { }

  private eventos: EventoInterface

  ngOnInit() {
    this.dataApi
      .getPremium()
      .subscribe((data: EventoInterface) => (this.eventos = data));
  }
}
