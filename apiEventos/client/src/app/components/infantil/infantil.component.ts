import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { EventoInterface } from "../../models/evento-interface";

@Component({
  selector: 'app-infantil',
  templateUrl: './infantil.component.html',
  styleUrls: ['./infantil.component.css']
})
export class InfantilComponent implements OnInit {

  constructor( private dataApi: DataApiService) { }

  private eventos: EventoInterface;

  ngOnInit() {
    this.getListEventos();
  }

  getListEventos(){
    this.dataApi.getInfantil().subscribe((eventos: EventoInterface) => (this.eventos = eventos));
  }

}
