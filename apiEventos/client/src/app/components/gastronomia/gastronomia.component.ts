import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { EventoInterface } from "../../models/evento-interface";

@Component({
  selector: 'app-gastronomia',
  templateUrl: './gastronomia.component.html',
  styleUrls: ['./gastronomia.component.css']
})
export class GastronomiaComponent implements OnInit {

  constructor( private dataApi: DataApiService) { }

  private eventos: EventoInterface;

  ngOnInit() {
    this.getListEventos();
  }

  getListEventos(){
    this.dataApi.getGastronomia().subscribe((eventos: EventoInterface) => (this.eventos = eventos));
  }

}
