export interface EventoInterface{
    id?: string;
    titulo?: string;
    descripcion?: string;
    imagen?: string;
    categoria?: string;
    fecha?: Date;
}