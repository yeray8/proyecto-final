## PROJECTE FINAL DEL CFGS ASIX
### IES EMILI DARDER

---

## Què és el projecte final d'ASIX?

El projecte final de mòdul és un mòdul de caràcter obligatori del CFGS amb el que "es
pretén complementar la formació dels mòduls del cicle en les funcions d'anàlisi, disseny i
planificació d'aquests" **(BOE núm 123, 20 Maig 2010)**.

Aquest projecte serà un treball on es recullin els coneixements adquirits al llarg dels dos
anys cursats en el cicle en qüestió. Depenent de l'especialitat del mòdul els continguts
podran diferenciar-se, però tots aniran encaminats a realitzar treballs d'investigació o
desenvolupament.

Les capacitats que es cerca desenvolupar amb la realització del mateix són:

* Capacitats d'anàlisi i recerca d'informació rellevant
* Disseny de solucions efectives a problemes reals presentats,
* Organització,
* L'execució de treballs en equip,
* L'autonomia i la iniciativa.

---

## Quines característiques ha de tenir el projecte?

* El mòdul professional de projecte té un caràcter interdisciplinari i incorpora les
variables tecnològiques i organitzatives relacionades amb els aspectes essencials
de la competència professional del títol de Tècnic Superior en Administració de
Sistemes Informàtics en Xarxa (ASIX).
* Amb caràcter general aquest mòdul serà impartit pel tutor de formació en centres
de treball.
* El mòdul professional de projecte es desenvoluparà durant el mateix període que
el mòdul professional de formació en centres de treball. El professor responsable
del seu desenvolupament haurà anticipar les activitats d'ensenyament i
aprenentatge que facilitin el desenvolupament posterior del mòdul.
* El desenvolupament i seguiment del mòdul professional de projecte haurà de
compaginar la tutoria individual i col·lectiva. En qualsevol cas, almenys el 50% de
la durada total es durà a terme de forma presencial, completant-se amb la tutoria
a distància emprant les tecnologies de la informació i la comunicació.
* L'avaluació d'aquest mòdul professional queda condicionada a l'avaluació positiva
de la resta dels mòduls professionals del cicle formatiu, inclòs el de formació en
centres de treball.

---

## Projecte a presentar amb els següents requisits:

**Aplicació web**

* L'arquitectura de l'aplicació web,
* L'arquitectura de la base de dades,
* El desplegament de l'aplicació,
* La securització del sistema.
* El projecte tindrà que incloure persistencia en base de dades


---
***Documentació by*** : Yeray Millán